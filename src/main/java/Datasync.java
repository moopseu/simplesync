import datagram.datagram;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class Datasync extends Thread{


    Thread T;
    static String username1;
    static String home;
    static Socket klijent = null;
    static boolean run = true;
    static boolean pokrenuto = false;
    static ProgressBar progress = null;
    static Label glavni,filename=null;


    private static void visak(DataOutputStream izlaz,datagram server) throws IOException {
        String ime = server.file.getName();
        izlaz.writeUTF("visak");
        izlaz.writeUTF(ime);
        System.out.println("visak na serveru : "+ ime);
        Platform.runLater(() -> glavni.setText("Visak na serveru"));
        Platform.runLater(()->filename.setText(ime));

    }

    private static void difference(DataOutputStream izlaz2, ObjectOutputStream izlaz, ObjectInputStream ulaz) throws IOException, ClassNotFoundException {
        File sync = new File(home);
        datagram[] home = datagram.stvoriop(sync.listFiles());
        datagram[] server = datagram.opisnik(izlaz2,ulaz);

        boolean exist;
        for(int i = 0; i < server.length; i++){

            exist = false;
            for(int j = 0; j<home.length;j++){

                if(server[i].Md5checksum.equals(home[j].Md5checksum)){
                    exist = true;
                }

            }


            if(!exist){
                visak(izlaz2,server[i]);
            }




        }

    }


    public void run(){
        DataOutputStream izlaz=null;
        ObjectInputStream ulaz=null;
        DataInputStream ulaz2=null;
        ObjectOutputStream izlaz2=null;
        try{
            klijent = new Socket("moops.ddns.net", 6933);
            izlaz = new DataOutputStream(klijent.getOutputStream());
            ulaz = new ObjectInputStream(klijent.getInputStream());
            ulaz2 = new DataInputStream(klijent.getInputStream());
            izlaz2 = new ObjectOutputStream(klijent.getOutputStream());

        }catch(Exception e){};
        File sync = new File(home);
        File[] podacidir = sync.listFiles();
        datagram[] a1= datagram.stvoriop(podacidir);


        System.out.println("Simplesync v 1.0 ");
        while(username1 == null)System.out.println("waiting");
        try {
            izlaz.writeUTF("pocetak");
            izlaz.writeUTF(username1);

        } catch (IOException e) {
            e.printStackTrace();
        }

        while(true && run) {
            try {
                syndir(izlaz2,izlaz, ulaz, ulaz2,sync);
            } catch (IOException e){
                e.printStackTrace();

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        if(run==false) {
            System.out.println("Zaustavio");
            T.interrupt();
            T = null;
        }
    }
    public void start(ProgressBar a,Label b,Label c){
        progress = a;
        glavni = b;
        filename = c;
        if (T == null)
        {
            T = new Thread(this);
            T.start ();
        }
        run=true;


    }




    private static void syndir(ObjectOutputStream izlaz2,DataOutputStream izlaz, ObjectInputStream ulaz,DataInputStream dis,File sync) throws IOException, ClassNotFoundException {

        try {

            Path path = Paths.get(home);
            File[] homeopisnik = sync.listFiles();
            datagram[] server=datagram.opisnik(izlaz, ulaz);
            datagram[] client=datagram.stvoriop(homeopisnik);

            if (!pokrenuto || !Files.exists(path)) {
                System.out.println("usao tu");
                server = datagram.opisnik(izlaz, ulaz);
                if(!Files.exists(path)){
                    sync.mkdir();
                }
                izlaz.writeUTF("daj file");
                int duljina = dis.readInt();
                System.out.print("Initialdownload "+"\n"+"broj podataka u datoteci: " + duljina + "\n");
                int n = 0;
                byte[] buf = new byte[4092];
                System.out.println(home);
                for (int i = 0; i < duljina; i++) {
                    long duljinapod = dis.readLong();
                    long dpod = duljinapod;
                    Platform.runLater(() -> glavni.setText("Initial download: "));
                    System.out.println(server[i].file.getName() + "-" + server[i].Md5checksum + " - " + duljinapod + " bytes");
                    final String servername,MD5;
                    servername = server[i].file.getName();
                    MD5 = server[i].Md5checksum;
                    final long poddulj = duljinapod;
                    Platform.runLater(() -> filename.setText(servername + "-"+ poddulj/1000000 + "MB"));
                    Platform.runLater(() -> filename.setVisible(true));
                    FileOutputStream fos = new FileOutputStream(home + "\\" + server[i].file.getName());
                    progress.setVisible(true);
                    while (duljinapod > 0 && (n = dis.read(buf, 0, (int) Math.min(buf.length, duljinapod))) != -1) {
                        fos.write(buf, 0, n);
                        fos.flush();
                        duljinapod -= n;
                        double omjer = (double) duljinapod / (double) dpod;
                        omjer = - (omjer-1);
                        progress.setProgress(omjer);
                    }
                    fos.close();
                }
                Platform.runLater(() -> glavni.setText("Download finished"));
                pokrenuto = true;
            }


            if(server.length == client.length){
                difference(izlaz,izlaz2,ulaz);
            }


            if(!sync.isDirectory())return;
            homeopisnik = sync.listFiles();
            server = datagram.opisnik(izlaz, ulaz);
            client = datagram.stvoriop(homeopisnik);


            if (pokrenuto && Files.exists(path) && Files.isDirectory(path) && (server.length > client.length) && homeopisnik != null) {


                for (int i = 0; i < server.length; i++) {
                    boolean postoji = false;
                    for (int j = 0; j < homeopisnik.length; j++) {
                        if (server[i].Md5checksum.equals(client[j].Md5checksum)) {
                            postoji = true;
                        }
                    }
                    if (!postoji) {
                        visak(izlaz,server[i]);
                    }
                    if(!sync.isDirectory())return;
                    homeopisnik = sync.listFiles();
                    server = datagram.opisnik(izlaz, ulaz);
                    client = datagram.stvoriop(homeopisnik);

                }

            }





            if (pokrenuto && Files.exists(path) && Files.isDirectory(path) && (homeopisnik.length > server.length && homeopisnik != null) && homeopisnik.length!= 0) {
                if(!sync.isDirectory())return;
                server = datagram.opisnik(izlaz, ulaz);
                homeopisnik = sync.listFiles();
                client = datagram.stvoriop(homeopisnik);


                for (int i = 0; i < client.length; i++) {
                    boolean postoji = false;
                    for (int j = 0; j < server.length; j++) {
                        if (client[i].Md5checksum.equals(server[j].Md5checksum) && !homeopisnik[i].isDirectory()  ) {
                            postoji = true;
                        }
                    }
                    if (postoji == false && !client[i].file.isDirectory()) {
                        int n = 0;
                        byte[] bufer = new byte[4096];
                        izlaz.writeUTF("upload file");
                        File upload = homeopisnik[i];
                        System.out.println("upload file - "+upload.getName()+" - " + client[i].Md5checksum);
                        Platform.runLater(() -> glavni.setText("Uploading"));
                        long duljina = upload.length();
                        String ime = upload.getName();
                        izlaz.writeLong(duljina);
                        izlaz.writeUTF(ime);

                        final String servername,MD5;
                        servername = client[i].file.getName();
                        MD5 = client[i].Md5checksum;
                        final long poddulj = duljina;
                        Platform.runLater(() -> filename.setText(servername + "-" + poddulj/1000000 + " MB"));
                        filename.setVisible(true);
                        progress.setVisible(true);
                        long sum = 0;
                        FileInputStream fis1 = new FileInputStream(upload);
                        while ((n = fis1.read(bufer)) != -1) {
                            izlaz.write(bufer, 0, n);
                            izlaz.flush();
                            sum += n;
                            progress.setProgress((double)sum/(double)duljina);
                        }
                        fis1.close();


                    }

                }
                Platform.runLater(() -> glavni.setText("Upload finished"));

            }
        }catch(Exception e){

        }



    }
}